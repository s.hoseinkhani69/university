# دانشگاه 
این پروژه شامل دروس است که به صورت زیر مجموعه ایی
 به هم ارتباط دارندمثل درس ریاضی یک که پیش نیاز 
ریاضی 2 است  یک سری دانشجو که
 میتوانند این دروس را اخذ کنند
 تا پیش نیاز ها نمره شان ثبت نشود
 نمیتواند دروس بعدی آنها اخذ شود

## منوی اصلی
[ثبت نام دانشجو](https://www.example.com)

[ثبت دروس](https://www.example.com)

[ثبت نام اساتید](https://www.example.com)

## منوی دانشجو
در منوی دانشجو حتما باید ثبت نام شود
و در منوی خود می تواند درس جدید را انتخاب کند و دروس با توجه به پیش نیاز بودن اگر نمره درس پیش نیاز ثبت نشده باشد خطای درس پیش نیاز نمایش داده شود

## منوی دروس
درس شامل نام و  دروس پیش نیاز می باشد
## منوی اساتید
اطلاعات اساتید شامل نام، نام خانوادگی،شماره پرسنلی  اضافه ،حذف و ویرایش می شود
## منوی انتخاب درس
در این منو آیدی دانشجو و آیدی استاد و آیدی درس و نمره ثبت می شود
## جداول مورد نیاز
1. جدول دانشجویان
2. جدول اساتید
3. جدول دروس
4. جدول انتخاب درس