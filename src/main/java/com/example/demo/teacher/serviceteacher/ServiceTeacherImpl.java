package com.example.demo.teacher.serviceteacher;

import com.example.demo.Student.contorollerstudent.StudentModel;
import com.example.demo.Student.persistant.Student;
import com.example.demo.base.ThrowException;
import com.example.demo.teacher.contorollerteacher.TeacherModel;
import com.example.demo.teacher.persistens.Teacher;
import com.example.demo.teacher.persistens.TeacherRepasitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
@Qualifier("ServiceTeacherImpl")
public class ServiceTeacherImpl implements ServiceTeacher {
    TeacherRepasitory teacherRepasitory;

    @Autowired
    public ServiceTeacherImpl(TeacherRepasitory teacherRepasitory) {
        this.teacherRepasitory = teacherRepasitory;
    }

    @Override
    public List<TeacherModel> index() {
        return teacherRepasitory.findAll().stream().map(ServiceTeacherImpl::converToModel).collect(Collectors.toList());
    }

    @Override
    public TeacherModel create(TeacherModel teacherModel) {

        teacherRepasitory.save(convertToEntity(teacherModel));

        return teacherModel;
    }

    @Override
    public TeacherModel update(TeacherModel teacherModel) {
        Teacher teacher2=convertToEntity(teacherModel);
        try {

            Teacher teacherDB = teacherRepasitory.findById(teacher2.getId()).orElseThrow(() -> new ThrowException("teacher Not Found"));

        if( teacher2.getName()!=null && !(teacher2.getName().equals(teacherDB.getName()))){
            teacherDB.setName(teacher2.getName());
        }
        if( teacher2.getFamily()!=null&&!(teacher2.getFamily().equals(teacherDB.getFamily()))){
            teacherDB.setFamily(teacher2.getFamily());
        }
        if( teacher2.getAddress()!=null&&!(teacher2.getAddress().equals(teacherDB.getAddress()))){
            teacherDB.setAddress(teacher2.getAddress());
        }
        if( teacher2.getAddress()!=null&&!(teacher2.getAddress().equals(teacherDB.getAddress()))){
            teacherDB.setAddress(teacher2.getAddress());
        }
        if( teacher2.getCodeMeli()!=null&&!(teacher2.getCodeMeli().equals(teacherDB.getCodeMeli()))){
            teacherDB.setCodeMeli(teacher2.getCodeMeli());
        }

        return converToModel(teacherRepasitory.save(teacherDB));
        }catch (Exception e){return teacherModel;}
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            teacherRepasitory.deleteById(id);
            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    @Override
    public TeacherModel showOneById(Integer id) {
        return converToModel(teacherRepasitory.findById(id).orElseThrow(()->new ThrowException("Not Found Teacher")));
    }

   @Override
    public TeacherModel findTeacherByCodeMeli(String code) {
        return converToModel(teacherRepasitory.findTeacherByCodeMeli(code).orElseThrow(()->new ThrowException("NOT Found Teacher")));
    }


    public static TeacherModel converToModel(Teacher teacher) {
        TeacherModel teacherModel = new TeacherModel();
        teacherModel.setId(teacher.getId());
        teacherModel.setName(teacher.getName());
        teacherModel.setFamily(teacher.getFamily());
        teacherModel.setCode_meli(teacher.getCodeMeli());
        teacherModel.setAddress(teacher.getAddress());
        return teacherModel;
    }

    public static Teacher convertToEntity(TeacherModel teacherModel) {
        Teacher teacher = new Teacher();
        teacher.setId(teacherModel.getId());
        teacher.setName(teacherModel.getName());
        teacher.setFamily(teacherModel.getFamily());
        teacher.setCodeMeli(teacherModel.getCode_meli());
        teacher.setAddress(teacherModel.getAddress());
        return teacher;
    }
}
