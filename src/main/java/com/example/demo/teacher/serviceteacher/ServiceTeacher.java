package com.example.demo.teacher.serviceteacher;

import com.example.demo.Student.contorollerstudent.StudentModel;
import com.example.demo.teacher.contorollerteacher.TeacherModel;
import com.example.demo.teacher.persistens.Teacher;

import java.util.List;

public interface ServiceTeacher {

    List<TeacherModel> index();

    TeacherModel create(TeacherModel teacherModel);

    TeacherModel update(TeacherModel teacherModel);

    Boolean delete(Integer id);

    TeacherModel showOneById(Integer id);

    TeacherModel findTeacherByCodeMeli(String code);
}