package com.example.demo.teacher.contorollerteacher;

import com.example.demo.Student.contorollerstudent.StudentModel;
import com.example.demo.Student.servicestudent.ServiceStudent;
import com.example.demo.teacher.serviceteacher.ServiceTeacher;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Teacher")
public class ContorollerTeacher {

    ServiceTeacher serviceTeacher;

    public ContorollerTeacher(ServiceTeacher serviceTeacher) {
        this.serviceTeacher = serviceTeacher;
    }

    @GetMapping(value = {"/"})
    private List<TeacherModel> index() {
        return serviceTeacher.index();
    }

    @GetMapping(value = {"/find-by-code/{code}"})
    private TeacherModel findOneByCodemeli(@PathVariable String code) {
        return serviceTeacher.findTeacherByCodeMeli(code);
    }
    @GetMapping(value = {"/{id}"})
    private TeacherModel showOneById(@PathVariable Integer id) {
        return serviceTeacher.showOneById(id);
    }

    @PostMapping(value = {"/", ""})
    private TeacherModel create(@RequestBody TeacherModel teacherModel) {
        return serviceTeacher.create(teacherModel);
    }

    @PutMapping(value = {"/", ""})
    private TeacherModel update(@RequestBody TeacherModel teacherModel) {
        return serviceTeacher.update(teacherModel);
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id) {
        return serviceTeacher.delete(id);
    }
}
