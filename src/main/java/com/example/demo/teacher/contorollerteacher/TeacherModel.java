package com.example.demo.teacher.contorollerteacher;

import lombok.Data;

@Data
public class TeacherModel {

    Integer id;

    String name;

    private String family;

    private String code_meli;

    private String address;
}
