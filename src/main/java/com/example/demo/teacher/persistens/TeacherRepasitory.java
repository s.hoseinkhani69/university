package com.example.demo.teacher.persistens;

import com.example.demo.Student.persistant.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeacherRepasitory extends JpaRepository<Teacher,Integer> {
    Optional<Teacher> findById(Integer id);
    Optional<Teacher> findTeacherByCodeMeli(String code);

}
