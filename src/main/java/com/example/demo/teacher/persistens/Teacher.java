package com.example.demo.teacher.persistens;

import com.example.demo.Score.Persistanse.Score;
import com.example.demo.base.RunCounfiguration;
import com.example.demo.base.person;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "teacher",schema = RunCounfiguration.DB)
@Data
public class Teacher extends person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teachers")
    Collection<Score> ScoreTeacherCollection;

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", ScoreTeacherCollection=" + ScoreTeacherCollection +
                '}';
    }
}