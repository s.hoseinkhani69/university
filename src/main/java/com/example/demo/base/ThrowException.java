package com.example.demo.base;

import lombok.Data;

@Data
public class ThrowException extends RuntimeException{
    private String massage;

    public ThrowException(String massage) {
        this.massage = massage;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}