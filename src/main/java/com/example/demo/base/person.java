package com.example.demo.base;

import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public class person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name",nullable = false)
    private String name;
    @Column(name = "family",nullable = false)
    private String family;
    @Column(name = "code_meli",length = 10,unique = true,nullable = false)
    private String codeMeli;
    @Column(name = "address")
    private String address;

    @Override
    public String toString() {
        return "person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", family='" + family + '\'' +
                ", code_meli='" + codeMeli + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
