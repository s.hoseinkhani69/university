package com.example.demo.base;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ThrowException.class)
    public ResponseEntity<Object> handleThrowException(Exception ex){

        ApiError apiError = new ApiError (HttpStatus.NOT_FOUND, LocalDateTime.now(),((ThrowException) ex).getMassage());
              return new ResponseEntity<>(apiError,apiError.getStatus());
    }
}
