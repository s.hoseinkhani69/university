package com.example.demo.Student.persistant;

import com.example.demo.Student.contorollerstudent.StudentModel;
import com.example.demo.teacher.persistens.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    Student findOneById(Integer id);
    Optional<Student> findStudentByCodeMeli(String code);
}
