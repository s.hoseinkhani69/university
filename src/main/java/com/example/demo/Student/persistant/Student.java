package com.example.demo.Student.persistant;

import com.example.demo.Score.Persistanse.Score;
import com.example.demo.base.RunCounfiguration;
import com.example.demo.base.person;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "student",schema =RunCounfiguration.DB)
@Data
public class Student extends person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "students")
    Collection<Score> ScoreStudentCollection;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", ScoreStudentCollection=" + ScoreStudentCollection +
                '}';
    }
}
