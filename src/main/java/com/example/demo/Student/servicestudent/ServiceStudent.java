package com.example.demo.Student.servicestudent;

import com.example.demo.Lesson.contorollerlesson.LesseonModel;
import com.example.demo.Student.contorollerstudent.StudentModel;
import com.example.demo.Student.persistant.Student;
import org.springframework.data.jdbc.repository.query.Query;

import java.util.List;

public interface ServiceStudent {

    List<StudentModel> index();

    StudentModel create(StudentModel studentModel);

    StudentModel update(StudentModel studentModel);

    Boolean delete(Integer id);

    StudentModel showOneById(Integer id);

    StudentModel findStudentByCodeMeli(String code);
}

