package com.example.demo.Student.servicestudent;

import com.example.demo.Student.contorollerstudent.StudentModel;
import com.example.demo.Student.persistant.Student;
import com.example.demo.Student.persistant.StudentRepository;
import com.example.demo.base.ThrowException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
@Qualifier("ServiceStudentImpl")
public class ServiceStudentImpl implements ServiceStudent {
    StudentRepository studentRepository;

    @Autowired
    public ServiceStudentImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<StudentModel> index() {
        return studentRepository.findAll().stream().map(ServiceStudentImpl::convertToModel).collect(Collectors.toList());
    }

    @Override
    public StudentModel create(StudentModel studentModel) {
        studentRepository.save(this.convertToEntity(studentModel));
        return studentModel;
    }

    @Override
    public StudentModel update(StudentModel modelStudent) {
        Student student2 = convertToEntity(modelStudent);
        Student studentInDB = studentRepository.findById(student2.getId()).orElseThrow(() -> new ThrowException("Not Found student"));
        if (student2.getName() != null && !(student2.getName().equals(studentInDB.getName()))) {
            studentInDB.setName(student2.getName());
        }
        if (student2.getFamily() != null && !(student2.getFamily().equals(studentInDB.getFamily()))) {
            studentInDB.setFamily(student2.getFamily());
        }
        if (student2.getAddress() != null && !(student2.getAddress().equals(studentInDB.getAddress()))) {
            studentInDB.setAddress(student2.getAddress());
        }
        if (student2.getAddress() != null && !(student2.getAddress().equals(studentInDB.getAddress()))) {
            studentInDB.setAddress(student2.getAddress());
        }
        if (student2.getCodeMeli() != null && !(student2.getCodeMeli().equals(studentInDB.getCodeMeli()))) {
            studentInDB.setCodeMeli(student2.getCodeMeli());
        }

        return convertToModel(studentRepository.save(studentInDB));
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            studentRepository.deleteById(id);
            return Boolean.TRUE;
        } catch (Exception e) {
            return Boolean.FALSE;
        }
    }

    @Override
    public StudentModel showOneById(Integer id) {
        return convertToModel(studentRepository.findById(id).orElseThrow());
    }

    @Override
    public StudentModel findStudentByCodeMeli(String code) {
        return convertToModel(studentRepository.findStudentByCodeMeli(code).orElseThrow(()->new ThrowException("Not Found student")));
    }

    public static StudentModel convertToModel(Student student) {
        StudentModel studentModel = new StudentModel();
        studentModel.setId(student.getId());
        studentModel.setName(student.getName());
        studentModel.setFamily(student.getFamily());
        studentModel.setCode_meli(student.getCodeMeli());
        studentModel.setAddress(student.getAddress());
        return studentModel;
    }

    public static Student convertToEntity(StudentModel studentModel) {
        Student student = new Student();
        student.setId(studentModel.getId());
        student.setName(studentModel.getName());
        student.setFamily(studentModel.getFamily());
        student.setCodeMeli(studentModel.getCode_meli());
        student.setAddress(studentModel.getAddress());
        return student;
    }

}