package com.example.demo.Student.contorollerstudent;

import com.example.demo.Score.ContorollerScore.ScoreModel;
import com.example.demo.Score.ServiseScore.ServiceScore;
import com.example.demo.Student.servicestudent.ServiceStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class contorollerStudent {

    ServiceStudent serviceStudent;

    @Autowired
    public contorollerStudent(ServiceStudent serviceStudent) {
        this.serviceStudent = serviceStudent;
    }

    @GetMapping(value = {"/"})
    private List<StudentModel> index() {
        return serviceStudent.index();
    }

    @GetMapping(value = {"/{id}"})
    private StudentModel show(@PathVariable Integer id) {
        return serviceStudent.showOneById(id);
    }

    @GetMapping(value = {"/Find_By_CodeMeli/{code}"})
    private StudentModel findStudentByCodeMeli(@PathVariable String code) {
        return serviceStudent.findStudentByCodeMeli(code);
    }

    @PostMapping(value = {"/", ""})
    private StudentModel create(@RequestBody StudentModel studentModel) {
        return serviceStudent.create(studentModel);
    }

    @PutMapping(value = {"/", ""})
    private StudentModel update(@RequestBody StudentModel studentModel) {
        return serviceStudent.update(studentModel);
    }

    @DeleteMapping(value = {"/{id}", ""})
    public Boolean delete(@PathVariable Integer id) {
        return serviceStudent.delete(id);
    }

}

