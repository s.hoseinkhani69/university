package com.example.demo.Student.contorollerstudent;

import lombok.Data;
@Data
public class StudentModel {

    Integer id;

    String name;

    private String family;

    private String code_meli;

    private String address;

}
