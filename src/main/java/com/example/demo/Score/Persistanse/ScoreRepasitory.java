package com.example.demo.Score.Persistanse;

import org.hibernate.annotations.Where;
import org.hibernate.mapping.Value;
import org.hibernate.sql.Select;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jdbc.repository.query.Query;

import javax.persistence.criteria.From;

//import javax.management.Query;

@Repository
public interface ScoreRepasitory extends JpaRepository<Score, Integer> {
    Score findOneByStudentsIdAndLessonsId(Integer studentId, Integer lessonId);
}
