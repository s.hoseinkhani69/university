package com.example.demo.Score.Persistanse;

import com.example.demo.Lesson.persistence.Lesson;
import com.example.demo.base.RunCounfiguration;
import com.example.demo.Student.persistant.Student;
import com.example.demo.teacher.persistens.Teacher;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Score", schema = RunCounfiguration.DB)
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    @Column(name = "Score")
    Integer Score;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    Student students;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    Teacher teachers;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lesson_id", referencedColumnName = "id")
    Lesson lessons;

    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", Score=" + Score +
                ", students=" + students +
                ", teachers=" + teachers +
                ", lessons=" + lessons +
                '}';
    }
}
