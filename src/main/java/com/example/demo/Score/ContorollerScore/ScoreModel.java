package com.example.demo.Score.ContorollerScore;

import com.example.demo.Lesson.persistence.Lesson;
import com.example.demo.Student.persistant.Student;
import com.example.demo.teacher.persistens.Teacher;
import lombok.Data;
import org.springframework.data.relational.core.sql.In;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;

@Data
public class ScoreModel {
    Integer id;
    Integer score;
    //ScoreStudentModel scoreStudentModel;
    Integer Student_id;
    Integer Teacher_id;
    Integer Lesson_id;
}
