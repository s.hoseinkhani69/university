package com.example.demo.Score.ContorollerScore;

import com.example.demo.Lesson.contorollerlesson.LesseonModel;
import com.example.demo.Lesson.servicelesson.LessonService;
import com.example.demo.Score.Persistanse.Score;
import com.example.demo.Score.ServiseScore.ServiceScore;
import com.example.demo.Student.contorollerstudent.StudentModel;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/Score")
public class ContorollerScore {

    ServiceScore serviceScore;

    @Autowired
    public ContorollerScore(ServiceScore serviceScore) {

        this.serviceScore = serviceScore;
    }

    @GetMapping(value = {"/"})
    private List<ScoreModel> index() {
        return serviceScore.index();
    }

    @GetMapping(value = {"/{id}"})
    private ScoreModel show(@PathVariable Integer id) {
        return serviceScore.show(id);
    }

    @PostMapping(value = {"/", ""})
    private String create(@RequestBody ScoreModel scoreModel) {
        return serviceScore.create(scoreModel);
    }

    @PutMapping(value = {"/", ""})
    private ScoreModel update(@RequestBody ScoreModel scoreModel) {
        return serviceScore.update(scoreModel);
    }

    @DeleteMapping(value = {"/"})
    public Boolean delete(@PathVariable Integer id) {
        return serviceScore.delete(id);
    }
}

