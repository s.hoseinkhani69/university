package com.example.demo.Score.ServiseScore;

import com.example.demo.Lesson.persistence.Lesson;
import com.example.demo.Lesson.persistence.LessonRepasitory;
import com.example.demo.Score.ContorollerScore.ScoreModel;
import com.example.demo.Score.Persistanse.Score;
import com.example.demo.Score.Persistanse.ScoreRepasitory;
import com.example.demo.Student.persistant.Student;
import com.example.demo.Student.persistant.StudentRepository;
import com.example.demo.base.ThrowException;
import com.example.demo.teacher.persistens.Teacher;
import com.example.demo.teacher.persistens.TeacherRepasitory;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
@Qualifier("ServiceScoreImpl")
public class ServiceScoreImpl implements ServiceScore {

    ScoreRepasitory scoreRepasitory;
    StudentRepository studentRepository;
    TeacherRepasitory teacherRepasitory;
    LessonRepasitory lessonRepasitory;

    @Autowired
    public ServiceScoreImpl(ScoreRepasitory scoreRepasitory, StudentRepository studentRepository, TeacherRepasitory teacherRepasitory, LessonRepasitory lessonRepasitory) {
        this.scoreRepasitory = scoreRepasitory;
        this.studentRepository = studentRepository;
        this.teacherRepasitory = teacherRepasitory;
        this.lessonRepasitory = lessonRepasitory;
    }


    @Override
    public List<ScoreModel> index() {
        return scoreRepasitory.findAll().stream().map(this::convertToModel).collect(Collectors.toList());
    }

    @Override
    public String create(ScoreModel scoreModel) {
        String status;
        Score score = converScoreModelToEntity(scoreModel);
        if ((score.getLessons() != null) && (score.getStudents() != null)) {
            if (scoreRepasitory.findOneByStudentsIdAndLessonsId(score.getStudents().getId(),
                    score.getLessons().getId()) != null) {
                status = "نمره دانشجو برای این درس ثبت شده است ";
            } else if (score.getLessons().getParentId() != null) {
                Integer ParentId = score.getLessons().getParentId();
                Integer StudentId = score.getStudents().getId();
                Score score1 = scoreRepasitory.findOneByStudentsIdAndLessonsId(StudentId, ParentId);
                if (score1 != null) {
                    if (score1.getScore() >= 10) {
                        scoreRepasitory.save(score);
                        status = "درس پیش نیاز را گذرانده و درس جدید با موفقیت ثبت شد";
                    } else status = "پیش نیاز این درس نمره قبولی دریافت نکرده است";
                } else status = " پیش نیاز این درس گذرانده نشده است";
            } else {
                scoreRepasitory.save(score);
                status = "درس با موفقیت ثبت شد";
            }
        } else status = "درس وجود ندارد";

        return status;
    }

    @Override
    public ScoreModel update(ScoreModel scoreModel) {
        Score score = converScoreModelToEntity(scoreModel);

            Score scoreDB = scoreRepasitory.findById(score.getId()).orElseThrow(() -> new ThrowException("Not Found Lesson.."));

            if (scoreDB != null) {

                if (score.getScore() != null && !(score.getScore().equals(scoreDB.getScore()))) {
                    scoreDB.setScore(score.getScore());
                }
                scoreModel = convertToModel(scoreRepasitory.save(scoreDB));
            }

        return scoreModel;
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            scoreRepasitory.deleteById(id);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public ScoreModel show(Integer id) {
        return convertToModel(scoreRepasitory.findById(id).orElseThrow(() -> new ThrowException("Not Found ")));
    }


    public ScoreModel convertToModel(Score score) {
        ScoreModel scoreModel = new ScoreModel();
        scoreModel.setId(score.getId());
        scoreModel.setScore(score.getScore());
        scoreModel.setLesson_id(score.getLessons().getId());
        scoreModel.setTeacher_id(score.getTeachers().getId());
        scoreModel.setStudent_id(score.getStudents().getId());
        return scoreModel;
    }

    public Score converScoreModelToEntity(ScoreModel scoreModel) {
        Score score = new Score();
        score.setId(scoreModel.getId());
        try {
            score.setScore(scoreModel.getScore());
        } catch (Exception e) {
            score.setScore(null);
        }
        if (scoreModel.getStudent_id() != null&&scoreModel.getLesson_id()!=null&&scoreModel.getTeacher_id()!=null) {
            Student student = studentRepository.findById(scoreModel.getStudent_id()).
                    orElseThrow(() -> new ThrowException("Not Found student"));
            score.setStudents(student);

            Teacher teacher = teacherRepasitory.findById(scoreModel.getTeacher_id())
                    .orElseThrow(() -> new ThrowException("Not Found teacher"));
            score.setTeachers(teacher);

            Lesson lesson = lessonRepasitory.findById(scoreModel.getLesson_id())
                    .orElseThrow(() -> new ThrowException("Not Found lesson"));
            score.setLessons(lesson);
        }else throw new ThrowException("Not Found...");
        return score;
    }

}
