package com.example.demo.Score.ServiseScore;

import com.example.demo.Lesson.contorollerlesson.LesseonModel;
import com.example.demo.Score.ContorollerScore.ScoreModel;
import com.example.demo.Score.Persistanse.Score;
import com.example.demo.Student.persistant.Student;
import javassist.NotFoundException;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Collection;
import java.util.List;

public interface ServiceScore {
    List<ScoreModel> index();

    String create(ScoreModel scoreModel);

    ScoreModel update(ScoreModel scoreModel);

    Boolean delete(Integer id);

    ScoreModel show(Integer id);
}
