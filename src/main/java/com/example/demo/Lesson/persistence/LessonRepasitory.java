package com.example.demo.Lesson.persistence;

import com.example.demo.Student.persistant.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LessonRepasitory extends JpaRepository<Lesson, Integer> {
    Optional<Lesson> findOneByName(String Name);

    Lesson findOneById(Integer id);

    Optional<Boolean> deleteByName(String name);
}
