package com.example.demo.Lesson.persistence;

import com.example.demo.Score.Persistanse.Score;
import com.example.demo.base.RunCounfiguration;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "Lesson", schema = RunCounfiguration.DB)
@Data
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "Parent_Id")
    private Integer ParentId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "lessons")
    Collection<Score> ScoreLessonCollection;

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", Parent_Id=" + ParentId +
                ", ScoreLessonCollection=" + ScoreLessonCollection +
                '}';
    }
}