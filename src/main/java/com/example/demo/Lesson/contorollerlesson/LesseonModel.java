package com.example.demo.Lesson.contorollerlesson;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LesseonModel {

    private Integer id;

    private String name;

    private Integer ParentId;

}
