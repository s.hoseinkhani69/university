package com.example.demo.Lesson.contorollerlesson;

import com.example.demo.Lesson.servicelesson.LessonService;
import com.example.demo.Score.ContorollerScore.ScoreModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Lesson")
public class LessonContoroller {

    private LessonService lessonService;

    public LessonContoroller(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    @GetMapping(value = {"/"})
    private List<LesseonModel> index() {
        return lessonService.index();
    }

    @GetMapping(value = {"/{id}"})
    private LesseonModel showOneById(@PathVariable Integer id) {
        return lessonService.showOneById(id);
    }

    @GetMapping(value = {"/Find-By-name/{name}"})
    private LesseonModel findByName(@PathVariable String name) {
        return lessonService.findOneByName(name);
    }

    @PostMapping(value = {"/", ""})
    private LesseonModel create(@RequestBody LesseonModel lesseonModel) {
        return lessonService.create(lesseonModel);
    }

    @PutMapping(value = {"/", ""})
    private LesseonModel update(@RequestBody LesseonModel lesseonModel) {
        return lessonService.update(lesseonModel);
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id) {
        return lessonService.delete(id);
    }

    @DeleteMapping({"/delete-by-name/{name}"})
    public Boolean deleteByName(@PathVariable String name) {
        return lessonService.deleteByName(name);
    }


}
