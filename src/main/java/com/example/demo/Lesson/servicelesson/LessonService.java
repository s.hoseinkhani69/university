package com.example.demo.Lesson.servicelesson;

import com.example.demo.Lesson.contorollerlesson.LesseonModel;
import com.example.demo.Lesson.persistence.Lesson;

import java.util.List;

public interface LessonService {

    List<LesseonModel> index();

    LesseonModel showOneById(Integer id);

    LesseonModel findOneByName(String Name);

    LesseonModel create(LesseonModel lesseonModel);

    LesseonModel update(LesseonModel lesseonModel);

    Boolean delete(Integer id);

    Boolean deleteByName(String name);
}
