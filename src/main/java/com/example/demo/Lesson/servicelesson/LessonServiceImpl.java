package com.example.demo.Lesson.servicelesson;

import com.example.demo.Lesson.contorollerlesson.LesseonModel;
import com.example.demo.Lesson.persistence.Lesson;
import com.example.demo.Lesson.persistence.LessonRepasitory;
import com.example.demo.base.ThrowException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional(rollbackFor = Exception.class)
@Qualifier("LessonServiceImpl")
public class LessonServiceImpl implements LessonService {

    private LessonRepasitory lessonRepasitory;

    @Autowired
    public LessonServiceImpl(LessonRepasitory lessonRepasitory) {
        this.lessonRepasitory = lessonRepasitory;
    }


    @Override
    public List<LesseonModel> index() {
        return lessonRepasitory.findAll().stream().map(LessonServiceImpl::convertToModel).collect(Collectors.toList());
    }

    @Override
    public LesseonModel showOneById(Integer id) {
        return convertToModel(lessonRepasitory.findById(id).orElseThrow(() -> new ThrowException("Not Found Lesson")));
    }

    @Override
    public LesseonModel create(LesseonModel lesseonModel) {
        Lesson lesson = convertToEntity(lesseonModel);
        return convertToModel(lessonRepasitory.save(lesson));
    }

    @SneakyThrows
    @Override
    public LesseonModel update(LesseonModel lesseonModel) {
        Lesson lessonDB = lessonRepasitory.findById(lesseonModel.getId()).orElseThrow(() -> new ThrowException("Not Found"));
        Lesson lesson1 = convertToEntity(lesseonModel);
        if (lesson1.getName() != null && !(lesson1.getName().equals(lessonDB.getName()))) {
            lessonDB.setName(lesson1.getName());
        }
        if (lesson1.getParentId() != null && !(lesson1.getParentId().equals(lessonDB.getParentId()))) {
            lessonDB.setParentId(lesson1.getParentId());
        }
        return convertToModel(lessonRepasitory.save(lessonDB));

    }

    @Override
    public Boolean delete(Integer id) {
        try {
            lessonRepasitory.deleteById(id);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public LesseonModel findOneByName(String Name) {
        return convertToModel(lessonRepasitory.findOneByName(Name).orElseThrow(() -> new ThrowException("Not Found Lesson")));
    }

    @Override
    public Boolean deleteByName(String name) {
        try {
            lessonRepasitory.deleteByName(name);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public static LesseonModel convertToModel(Lesson lesson) {
        LesseonModel lesseonModel = new LesseonModel();
        lesseonModel.setId(lesson.getId());
        lesseonModel.setName(lesson.getName());
        lesseonModel.setParentId(lesson.getParentId());
        return lesseonModel;
    }

    public static Lesson convertToEntity(LesseonModel lesseonModel) {
        Lesson lesson = new Lesson();
        lesson.setId(lesseonModel.getId());
        lesson.setName(lesseonModel.getName());
        lesson.setParentId(lesseonModel.getParentId());
        return lesson;
    }
}
